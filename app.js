require('dotenv').config();
const express = require('express');
const http = require('http');
const db = require('./src/db/mongo');
const bodyParser = require('body-parser');

const app = express();
const api = require('./src/routes');
// const {getitems} = require('./ebay')

// const db = require('./db/mongo');
const PORT = 8080;

const server = http.createServer(app);

app.use(bodyParser.json());
app.use('/api', api);


server.listen(PORT, () => console.log(`server running on port ${PORT}`));
