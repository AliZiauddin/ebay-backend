const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const {excelToJson} = require('../../readingFile');
const ebay = require('../../ebay');
const Item = require('../models/items.model');
const LeastPriceItem = require('../models/leastPriceItem');
const ItemIndex = require('../models/currentIndex');
const { find, findOne } = require('../models/items.model');
const { count } = require('console');
const { searchItems } = require('ebay-node-api/src/buy-api');


let records = []

async function getItems(jsonData,j){
    if(!jsonData.upc) id = (jsonData.vendor_name + ' ' + jsonData.part_number)
    else id = (jsonData.upc)
    console.log("======= " +j +" ============" +" "+id)

    data = await ebay.findItemsAdvanced({
          keywords:id,
          // include
    });
    return data;
}

const sendEbayDatatoDb = async(excelData,records) => {
  if(!excelData.upc){
    let toRecord = {
      count: counts,
      brandName: excelData.vendor_name,
      partNumber: excelData.part_number,
      allItems: records
    }
    let isData = await Item.findOne({brandName:excelData.vendor_name, partNumber: excelData.part_number});
    if(isData) await Item.updateOne({_id: isData.id}, toRecord);
    else{
      let newRecord = new Item(toRecord);
      await newRecord.save();
    }
  }
  else{
    toRecord = {
      upc: excelData.upc,
      count: counts,
      brandName: excelData.vendor_name,
      partNumber: excelData.part_number,
      allItems: records
    }
    isData = await Item.findOne({upc:excelData.upc});
    if(isData) await Item.updateOne({_id: isData.id}, toRecord);
    else{
      let newRecord = new Item(toRecord);
      await newRecord.save();
    }
  }
}

function compare_price(a, b){
  // a should come before b in the sorted order
  if(parseFloat(a.currentPrice) < parseFloat(b.currentPrice)){
          return -1;
  // a should come after b in the sorted order
  }
  else if(parseFloat(a.currentPrice) > parseFloat(b.currentPrice)){
          return 1;
  // and and b are the same
  }else{
          return 0;
  }
  
}

const cleaningItem = (item,count, jsonData)=>{
    if(!jsonData.upc) id = ''
    else id = (jsonData.upc)
    if(!jsonData.vendor_name) brandname = ''
    else brandname = (jsonData.vendor_name)
    if(!item.shippingInfo[0].shippingServiceCost){
       shippingServicesCost = 'not available'
      }
    else shippingServicesCost= item.shippingInfo[0].shippingServiceCost[0].__value__
    
    
    return(
    
        {
          itemId:                   item.itemId[0], //itemID
          upc:                      id,
          count:                    count,
          brandName:                brandname,
          title:                    item.title[0], //title
          viewItemUrl:              item.viewItemURL[0],
          seller:                   item.sellerInfo[0].sellerUserName[0], //seller
          positivefeedback:         item.sellerInfo[0].positiveFeedbackPercent[0],
          topRatedSeller:           item.sellerInfo[0].topRatedSeller[0],
          shippingServiceCost:      shippingServicesCost,
          oneDayShippingAvailable:  item.shippingInfo[0].oneDayShippingAvailable[0],
          currentPrice:             item.sellingStatus[0].currentPrice[0].__value__,
          sellingState:             item.sellingStatus[0].sellingState[0],
          returnsAccepted:          item.returnsAccepted[0], 
          myCostPrice:              jsonData.cost,
          mySellingPrice:           jsonData.price,
          // location:                 item.location[0], //location
          // galleryUrl:               item.galleryURL[0], 
          // shippingServiceCost:      item.shippingInfo[0].shippingServiceCost[0].__value__,
        }
      );
    }
   


module.exports = {
  async getitemsFromEbay(req,res){
    try {
      let jsonData = excelToJson('./inventory/inventoryexcel.xlsx');
      res.status(200).json(
        {success:"Data sending to db"}//.searchResult[0].item }
      );
      console.log("data start to get inserted")
      for(let j=0;j<jsonData.length;j++) {
                    
        data = await getItems(jsonData[j],j);
        if(data[0].searchResult) {
            
            let items = data[0].searchResult[0].item;
            
            if(items) {
              counts = items.length;
              for(let i=0; i<items.length; i++){
                  const cleanedItem = cleaningItem(items[i],counts, jsonData[j]);
                  records.push(cleanedItem);      
              }

              records = records.sort(compare_price);
              await sendEbayDatatoDb(jsonData[j],records);
              

              // if (records.length > 4) records= records.slice(0,4)
              // recordsToSend.push(records);
              // records = [];
              
            }
         }
      } 
      const count = await Item.countDocuments();
      console.log("Whole data is inserted, count: ", count);

    } catch (err) {
      console.error(err);
      res.status(500).json(
        {
          errors: [err.toString()],
        },
        null
      );
    }
  },

  async getSelectedItems(req,res){
    try{
        let excelData = req.body.data;
        let recordsToSend = [];
        for(let j=0;j<excelData.length;j++) {
                    
          let data = await getItems(excelData[j],j);
          // console.log(data.searchResult[0])
          if(data[0].searchResult) {
              
              let items = data[0].searchResult[0].item;
        
              if(items) {
                counts = items.length;
                for(let i=0; i<items.length; i++){
                  // console.log(items[i].sellingStatus[0])
                  
                  // if(items[i].shippingInfo[0].shippingServiceCost){
                    const cleanedItem = cleaningItem(items[i],counts, excelData[j]);
                    records.push(cleanedItem);      
                  // }
                }
                // await Item.insertMany(records);
                records = records.sort(compare_price);
                await sendEbayDatatoDb(excelData[j],records);
                

                if (records.length > 4) records= records.slice(0,4)
                recordsToSend.push(records);
                records = [];
                
              }
           }
        }
        res.status(200).json({
          success: {
            records: recordsToSend
          }
        });
      } catch (err) {
        console.error(err);
        res.status(500).json(
          {
            errors: [err.toString()],
          },
          null
        );
    }
  },
  async getEbayItems(req,res){
    // destructure page and limit and set default values
    const { page = 1, limit = 10 } = req.body;
      
    try {
      // execute query with page and limit values
      const data = await Item.find().sort({"updatedAt": -1})
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();
      
      for(let i=0; i<data.length; i++){
        if(data[i].allItems.length > 4)
        {
          data[i].allItems.splice(4);
        }
      }
  
      // get total documents in the Posts collection 
      const count = await Item.countDocuments();
  
      // return response with posts, total pages, and current page
      res.status(200).json({
        success: {
          data,
          totalPages: Math.ceil(count / limit),
          currentPage: page
        }

      });
    } catch (err) {
      console.error(err);
      res.status(500).json(
        {
          errors: [err.toString()],
        },
        null
      );
    }

  },
  
  async searchItems(req,res){
    let upc = req.query;
    if(!upc.upc){
        res.status(401).json({
          success: {
              msg: "Please enter upc to search"
          }
      });
    }
  
    let isData = await Item.findOne(upc);
    if(!isData){
      res.status(401).json({
        success: {
            msg: "No result found"
        }
    });
    return;
    }
    if(isData.allItems.length > 4)
        {
          isData.allItems.splice(4);
        }
    

    res.status(200).json({
      success: {
          record: isData
      }
    });
  }
} 
