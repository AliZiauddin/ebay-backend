const formidable = require('formidable');
const {excelToJson} = require('../../readingFile');
const {unlink} = require('fs');
const ExcelData = require('../models/excelData');

const sendToExcelDb = async (excelData)=>{
  
  const count = await ExcelData.countDocuments();
  if(count == 0){
    // console.log("Inside sendtoExcelDb Document count: ", count);
    await ExcelData.insertMany(excelData);
  }
  else {
        // console.log("Inside sendtoExcelDb Document count: ", count);
        for(let j=0;j<excelData.length;j++){
          // console.log("Doc inserted", j+1 )
            if(!excelData[j].upc){
              isData = await ExcelData.findOne({vendor_name:excelData[j].vendor_name, part_number: excelData[j].part_number});
              if(isData) await ExcelData.updateOne({_id: isData.id}, excelData[j]);
              else{
                let newRecord = new ExcelData(excelData[j]);
                await newRecord.save();
              }
            }
            else{
              isData = await ExcelData.findOne({upc:excelData[j].upc});
              if(isData) await ExcelData.updateOne({_id: isData.id}, excelData[j]);
              else{
                let newRecord = new ExcelData(excelData[j]);
                await newRecord.save();
              }
            }
        }
    }
}

module.exports ={ 
    async uploadExcel(req, res) {
      // console.log('here in uploading excel'
      const form = formidable({ multiples: true });
    
      form.parse(req, async (err, fields, files) => {
          try {
              if (err) {
                  res.staus(401).json(err);
                  return;
              }
              if(!files.excelFile){
                res.status(401).json({
                  error: {
                    msg: "File not uploaded",
                  }
                  
                });
                return;
                
              }
              
              
              res.status(200).json({
                success: {
                  msg: "File uploaded",
                  // data: data
                }
              });

              var data = excelToJson(files.excelFile.path);
              console.log('sending to db')
              unlink(files.excelFile.path, (err)=> {
                if(err) console.log(err)}
                );
              // await ExcelData.insertMany(data);
              // ExcelData.updateMany({upc,})
             
              await sendToExcelDb(data);

          } catch (err) {
              console.error(err);
              res.status(500).json(
                {

                  errors: [err.toString()],
                },
              );
            }
          }
      );
    },

    async getExcelData(req, res){
        // destructure page and limit and set default values
        const { page = 1, limit = 10 } = req.body;
      
        try {
          // execute query with page and limit values
          const data = await ExcelData.find().sort({"updatedAt": -1})
            .limit(limit * 1)
            .skip((page - 1) * limit)
            .exec();
      
          // get total documents in the Posts collection 
          const count = await ExcelData.countDocuments();
      
          // return response with posts, total pages, and current page
          res.status(200).json({
            success: {
              data,
              totalPages: Math.ceil(count / limit),
              currentPage: page
            }
    
          });
        } catch (err) {
          console.error(err);
          res.status(500).json(
            {
              errors: [err.toString()],
            },
            null
          );
        }
      

    }


}