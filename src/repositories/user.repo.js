const User = require('../models/user.model');
const bcrypt = require('bcryptjs');
// const EmailVerification = require('../models/emailVerification.model');
//const nodemailer = require('nodemailer');
const { sign } = require('jsonwebtoken');
const { TOKEN_EXPIRATION_TIME } = require('../config');

module.exports = {
    
    async createUser(req, res) {
        const { firstName, lastName, password} = req.body;

        const email = req.body.email.toLowerCase();
        try {
            // See if user exists
            let user = await User.findOne({
                email: email,
            });

            if (user) {
                return res.status(409).json({
                    errors: [
                        {
                            code: 409,
                            msg: 'Email address taken',
                        },
                    ],
                });
            }

            user = new User({
                firstName,
                lastName,
                email,
                password,
            });

            // Encrypt password
            const salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(password, salt);

            // // Verification Token
            // let emailVerificationToken = new EmailVerification({
            //     _userId: user._id,
            //     token: crypto.randomBytes(16).toString('hex'),
            // });
            // await emailVerificationToken.save();

            // var transporter = nodemailer.createTransport({
            //     service: 'gmail',
            //     auth: {
            //         user: process.env.USERNAME_SENDMAIL,
            //         pass: process.env.PASSWORD,
            //     },
            // });
            // var mailOptions = {
            //     from: 'no-reply@salnaka.com',
            //     to: user.email,
            //     subject: 'Account Verification Token',
            //     text:
            //         'Hi ' +
            //         user.firstName +
            //         ',\n\n' +
            //         'Please verify your account by clicking the link: \nhttp://' +
            //         req.headers.host +
            //         '/api' +
            //         '/user' +
            //         '/confirmation/' +
            //         emailVerificationToken.token +
            //         '.\n',
            // };

            // await transporter.sendMail(mailOptions);

            await user.save();

            res.status(201).json({
                success: {
                    //msg: 'A verification link has been sent to your email. Please verify your account and sign in.',
                    msg: 'New user created'
                },
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({
                errors: [
                    {
                        code: 500,
                        msg: err.toString(),
                    },
                ],
            });
        }
    },

    async loginUser(req, res) {
        const { email, password } = req.body;

        try {
            // See if user exists
            // console.log('in login');
            const user = await User.findOne({
                email,
            });

            if (!user) {
                return res.status(401).json({
                    errors: [
                        {
                            code: 401,
                            msg: 'Invalid Credentials',
                        },
                    ],
                });
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (!isMatch) {
                return res.status(401).json({
                    errors: [
                        {
                            code: 401,
                            msg: 'Invalid Credentials',
                        },
                    ],
                });
            }

            // if (!user.verified) {
            //     return res.status(401).json({
            //         errors: [
            //             {
            //                 code: 401,
            //                 msg: 'Your account has not been verified.',
            //             },
            //         ],
            //     });
            // }

            // Return jsonwebtoken
            const payload = {
                user: {
                    _id: user._id,
                },
            };

            sign(
                payload,
                process.env.JWT_SECRET,
                {
                    expiresIn: TOKEN_EXPIRATION_TIME,
                },
                (err, token) => {
                    if (err) {
                        console.error(err);
                        return res.status(500).json({
                            error: err.toString(),
                        });
                    }

                    const userObject = user.toObject();

                    const success = {
                        token,
                        user: userObject,
                    };

                    res.status(200).json({
                        success,
                    });
                },
            );
        } catch (err) {
            console.error(err);
            res.status(500).json(
                {
                    errors: [err.toString()],
                },
                null,
            );
        }
    },

    async getProfile(req, res) {
        try {
            const id = req.user.id;

            let user = await User.findOne({
                _id: id,
            });
            
            if (!user) {
                return res.status(400).json({
                    errors: [
                        {
                            msg: 'Invalid Credentials',
                        },
                    ],
                });
            }

            const userObject = user.toObject();
            delete userObject['password'];

            const success = {
                user: userObject,
            };

            res.status(200).json({
                success,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({
                errors: [
                    {
                        code: 500,
                        msg: err.toString(),
                    },
                ],
            });
        }
    },

    async updateProfile(req, res) {
        try {
            const id = req.user.id;

            const {
                firstName,
                lastName,
           } = req.body;

            let updateQuery = {};

            if (firstName) updateQuery.firstName = firstName;
            if (lastName) updateQuery.lastName = lastName;
            
            const user = await User.findOneAndUpdate(
                {
                    _id: id,
                },
                updateQuery,
                {
                    new: true,
                },
            );

            const success = {
                user: user,
            };

            res.status(200).json({
                success,
            });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({
                errors: [
                    {
                        code: 500,
                        msg: err.toString(),
                    },
                ],
            });
        }
    },

    async changePassword(req, res) {
        try {
            const id = req.user.id;

            let { currentPassword, newPassword } = req.body;
            // console.log(currentPassword, newPassword);
            let user = await User.findOne({
                _id: id,
            });

            if (!user) {
                return res.status(400).json({
                    errors: [
                        {
                            msg: 'Invalid Credentials',
                        },
                    ],
                });
            }

            const isMatch = await bcrypt.compare(currentPassword, user.password);

            if (!isMatch) {
                return res.status(401).json({
                    errors: [
                        {
                            code: 401,
                            msg: 'Invalid Credentials',
                        },
                    ],
                });
            }

            const salt = await bcrypt.genSalt(10);
            newPassword = await bcrypt.hash(newPassword, salt);

            user = await User.findOneAndUpdate(
                {
                    _id: id,
                },
                {
                    password: newPassword,
                },
                {
                    new: true,
                },
            );

            const userObject = user.toObject();
            delete userObject['password'];
            delete userObject['__v'];

            const success = {
                user: userObject,
            };

            res.status(200).json({
                success,
            });
        } catch (err) {
            console.error(err);
            res.status(500).json({
                errors: [{ code: 500, msg: err.toString() }],
            });
        }
    },

};
