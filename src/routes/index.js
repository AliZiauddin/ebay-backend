const router = require("express").Router();
const itemsController = require("../controllers/items.controller");
const excelController = require("../controllers/excel.controller");
const userController = require("../controllers/user.controller");   
const {userAuth} = require('../middleware/authentication');



router.use("/user", userController );
router.use(userAuth);
router.use("/items", itemsController);

//upload excel file 
router.use("/excel", excelController);




module.exports = router;