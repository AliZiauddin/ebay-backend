const mongoose = require("mongoose");

const leastPriceItemSchema = new mongoose.Schema(
    {
          itemId: {
            type: String,
            required: true,
          },
          upc: {
            type: String,
            required: true
          },
          brandName: {
            type: String,
            required: true
          },
          title:{
            type: String,
            required: true,
          },
          location: {
            type: String,
            required: true
          },
          seller: {
            type: String,
            required: true
          },
          positivefeedback: {
            type: String,
            required: true
          },
          topRatedSeller: {
            type: String,
            required: true
          },
          shippingServiceCost: {
            type: String,
            required: true
          },
          oneDayShippingAvailable: {
            type: String,
            required: true
          },
          currentPrice: {
            type: String,
            required: true
          },
          sellingState: {
            type: String,
            required: true
          },
          returnsAccepted: {
            type: String,
            required: true
          },
                    
    }, 
);

module.exports = mongoose.model("leastPriceItem", leastPriceItemSchema);