const mongoose = require("mongoose");

const excelDataSchema = new mongoose.Schema(
    {
        vendor_name: {
            type: String,
            required: false
          },
        vcpn: {
            type: String,
            required: false
          },
        vendor_code: {
            type: String,
            required: false
          },
        part_number: {
            type: String,
            required: false
          },
        product_name: {
            type: String,
            required: false
          },
        price: {
            type: String,
            required: false
          },
        cost: {
            type: String,
            required: false
          },
        core: {
            type: String,
            required: false
          },
        upsable: {
            type: String,
            required: false
          },
        is_non_returnable: {
            type: String,
            required: false
          },
        prop_65_toxicity: {
            type: String,
            required: false
          },
        case_qty: {
            type: String,
            required: false
          },
        is_hazmat: {
            type: String,
            required: false
          },
        is_chemical: {
            type: String,
            required: false
          },
        special_order_inv: {
            type: String,
            required: false
          },
        aaia_code: {
            type: String,
            required: false
          },
        mpn: {
            type: String,
            required: false
          },
        upc: {
            type: String,
            required: false
          },
        IsOversized: {
            type: String,
            required: false
          },
        EastQty: {
            type: String,
            required: false
          },
        MidwestQty: {
            type: String,
            required: false
          },
        CaliforniaQty: {
            type: String,
            required: false
          },
        SoutheastQty: {
            type: String,
            required: false
          },
        PacificNWQty: {
            type: String,
            required: false
          },
        TexasQty: {
            type: String,
            required: false
          },
        GreatLakesQty: {
            type: String,
            required: false
          },
        TotalQty: {
            type: String,
            required: false
          },
        IsKit: {
            type: String,
            required: false
          },        
    }, 
    {timestamps:true}
);
excelDataSchema.index({upc:1})
module.exports = mongoose.model("excelData", excelDataSchema);
