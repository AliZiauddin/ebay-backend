const mongoose = require("mongoose");

const itemSchema = new mongoose.Schema(
    {
          count: {
            type: String,
            required: false
          },
          upc: {
            type: String,
            required: false
          },
          brandName: {
            type: String,
            required: false
          },
          partNumber: {
            type: String,
            required: false
          },
          allItems: [
            {
              itemId: {
                type: String,
                required: false,
              },
              upc: {
                type: String,
                required: false
              },
              count: {
                type: String,
                required: false
              },
              brandName: {
                type: String,
                required: false
              },
              title:{
                type: String,
                required: false,
              },
              viewItemUrl: {
                type: String,
                required: false
              },
              seller: {
                type: String,
                required: false
              },
              positivefeedback: {
                type: String,
                required: false
              },
              topRatedSeller: {
                type: String,
                required: false
              },
              shippingServiceCost: {
                type: String,
                required: false
              },
              oneDayShippingAvailable: {
                type: String,
                required: false
              },
              currentPrice: {
                type: String,
                required: false
              },
              sellingState: {
                type: String,
                required: false
              },
              returnsAccepted: {
                type: String,
                required: false
              },
              mySellingPrice: {
                type: String,
                required: false
              }, 
              myCostPrice: {
                type: String,
                required: false
              },
            }
          ],
    }, 
    {timestamps:true}
);

itemSchema.index({upc:1})
module.exports = mongoose.model("Item", itemSchema);