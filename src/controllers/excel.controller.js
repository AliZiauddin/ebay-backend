const express = require("express");
const router = express.Router();
const excelRepo = require("../repositories/excel.repo");
const {userAuth} = require('../middleware/authentication');


// @route   POST api/support/
router.use(userAuth);

router.post("/upload", excelRepo.uploadExcel);
router.post("/data", excelRepo.getExcelData );

module.exports = router;