const express = require("express");
const router = express.Router();
const itemsRepo = require("../repositories/items.repo");


router.get("/ebaydirectly", itemsRepo.getitemsFromEbay);
router.post("/", itemsRepo.getSelectedItems);
router.post("/ebay", itemsRepo.getEbayItems);
router.get('/search', itemsRepo.searchItems    );

module.exports = router;