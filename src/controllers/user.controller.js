const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/authentication');
const userValidators = require('../validators/user.validator');
const UserRepo = require('../repositories/user.repo');

router.post('/register', userValidators.validateRegistration(), UserRepo.createUser);

// router.get('/confirmation/:token', confirmationPost.confirmationPost);
// router.post('/resend', userValidators.validateEmail(), resendToken);

// @route   POST api/user/login
// @desc    Authenticate user & get token
// @access  Public
router.post('/login', userValidators.validateLogin(), UserRepo.loginUser);

router.use(authMiddleware.userAuth);

router.get('/', UserRepo.getProfile);

router.put('/', userValidators.validateUpdate(), UserRepo.updateProfile);
// router.put('/updateAccountInfo', userValidators.validateAccountInfoUpdate(), UserRepo.updateAccountInfo);

router.post('/changePassword', userValidators.validateChangePassword(), UserRepo.changePassword);


module.exports = router;
